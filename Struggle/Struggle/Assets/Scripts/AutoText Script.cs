﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AutoTextScript : MonoBehaviour
{

	void Start()
	{
		isinteracting = false;
		isfinished = false;
	}
	public static bool isinteracting = false;
	public static bool isfinished = false;
    public static string colortext;
    public static string closinglabel;


    public static void TypeText(Text textElement, string text, float time, AudioSource sound)
	{
		float characterDelay = time;
	isinteracting = true;
	isfinished = false;
	textElement.StartCoroutine(SetText(textElement, text, characterDelay, sound));
	}

	static IEnumerator SetText(Text textElement, string text, float characterDelay, AudioSource sound)
	{
	yield return new WaitForSeconds(characterDelay);
		for(int i=0; i<text.Length; i++)
		{
		if (sound != null) {
				sound.Play ();
			}
            if (text[i] != '<')
            {
                textElement.text += text[i];
                isinteracting = true;
                isfinished = false;
                yield return new WaitForSeconds(characterDelay);
            }
            else
            {
                while (text[i] != '>')
                {
                    textElement.text += text[i];
                    i++;
                }
                textElement.text += text[i];
                i++;
                while (text[i] != '<')
                {
                    colortext += text[i];
                    i++;
                }
                while (text[i] != '>')
                {
                    closinglabel += text[i];
                    i++;
                }
               closinglabel += text[i];
                for (int j = 0; j < colortext.Length; j++)
                {
                    if (j == 0)
                    {
                        if (sound != null)
                        {
                            sound.Play();
                        }
                        textElement.text += colortext[j];
                        textElement.text += closinglabel;
                        isinteracting = true;
                        isfinished = false;
                        yield return new WaitForSeconds(characterDelay);
                    }
                    else
                    {
                        if (sound != null)
                        {
                            sound.Play();
                        }
                        textElement.text = textElement.text.Substring(0, textElement.text.Length - closinglabel.Length);
                        textElement.text += colortext[j];
                        textElement.text += closinglabel;
                        isinteracting = true;
                        isfinished = false;
                        yield return new WaitForSeconds(characterDelay);
                    }
                   
                }
                colortext = "";
                closinglabel = "";
            }								
		}
    colortext = "";
    closinglabel = "";
	isinteracting = false;
	isfinished = true;
	}

	public static void StopText(Text textElement)
	{
        isinteracting = false;
        isfinished = true;
		textElement.StopAllCoroutines ();
	}
}

