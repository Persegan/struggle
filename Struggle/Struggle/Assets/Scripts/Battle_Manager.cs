﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Battle_Manager : MonoBehaviour {

    public float Croix_Speed;
    public float Croix_Attack;
    public float Croix_Deffense;
    public float Croix_Health;
    public float Croix_ATB;

    public float Croix_Base_Speed;
    public float Croix_Max_Attack;
    public float Croix_Base_Deffense;
    public float Croix_Max_Health;
    public float Croix_Max_ATB;
    public float Croix_Acceleration;

    public float Enemy_Speed;
    public float Enemy_Attack;
    public float Enemy_Deffense;
    public float Enemy_Health;
    public float Enemy_ATB;

    public float Enemy_Max_Speed;
    public float Enemy_Max_Attack;
    public float Enemy_Max_Deffense;
    public float Enemy_Max_Health;
    public float Enemy_Max_ATB;


    public Slider Croix_HP_Bar;
    public Slider Croix_ATB_Bar;

    public Slider Enemy_HP_Bar;
    public Slider Enemy_ATB_Bar;

    public GameObject Croix_Menu;
    public GameObject Croix_Skills;
    public GameObject Croix_Items;
    public GameObject Croix_Information_Panel;

    public GameObject Hand_Indicator;
    public GameObject Notification_Text;

    public GameObject Croix;
    public GameObject Enemy;

    public GameObject DamageNumber;

    private int Enemy_Phase = 1;

    private int Max_Whirlwind_Slow = 5;
    private int Current_Whirlwind_Slow;

    private int Max_Fireball_Duration = 5;
    private int Current_Fireball_Duration;

    private int Max_Lightguard_Duration = 8;
    private int Current_Lightguard_Duration;

    private int Max_StarBlazing_Duration = 5;
    private int Current_StarBlazing_Duration;

    private int Max_GuardUp_Duration = 8;
    private int Current_GuardUp_Duration;

    private int Guard_Crush_Max_Cooldown = 4;
    private int Guard_Crush_Current_Cooldown;

    private int Whirlwind_Max_Cooldown = 4;
    private int Whirlwind_Current_Cooldown;

    private int Meteor_Max_Cooldown = 9;
    private int Meteor_Current_Cooldown;

    private int Impact_Swing_Max_Cooldown = 9;
    private int Impact_Swing_Current_Cooldown;

    private int Accelerate_Max_Cooldown = 1;
    private int Accelerate_Current_Cooldown;

    private int Deccelerate_Max_Cooldown = 1;
    private int Deccelerate_Current_Cooldown;

    private int Guard_Max_Cooldown = 4;
    private int Guard_Current_Cooldown;

    public int potions = 12;
    public int herbs = 12;

    private bool player_is_guarding = false;
    private bool charging_justice_strike = false;

    //Cooldowns for Silas skills

    private int FireBall_Max_Cooldown = 4;
    private int FireBall_Current_Cooldown;

    private int GuardUp_Max_Cooldown = 5;
    private int GuardUp_Current_Cooldown;

    private int JusticeStrike_Max_Cooldown = 6;
    private int JusticeStrike_Current_Cooldown;

    private int StarBlazing_Max_Cooldown = 4;
    private int StarBlazing_Current_Cooldown;

    private int Determination_Max_Cooldown = 5;
    private int Determination_Current_Cooldown;

    private int LightGuard_Max_Cooldown = 5;
    private int LightGuard_Current_Cooldown;
    

    //notification text stuff
    private float Notification_text_Speed  = 17.0f;
    private float Notification_text_Lifetime = 3.0f;
    private float Notification_text_initial_time = 0.5f;
    private float Notification_text_distancetoaddx = 1000.0f;
    private float Notification_text_distancetoaddy = -10.0f;

    public enum Battle_State
    {
        Charging,
        Waiting
    }

    public Battle_State current_state;


    // Use this for initialization
    void Start () {
        Croix_Menu.SetActive(false);
        Croix_Skills.SetActive(false);
        Croix_Items.SetActive(false);
        Croix_Information_Panel.SetActive(false);
        Hand_Indicator.SetActive(false);
        current_state = Battle_State.Charging;
	
	}
	
	// Update is called once per frame
	void Update () {
        Random.seed = System.DateTime.Now.Millisecond;
        switch (current_state)
        {
            case (Battle_State.Waiting):
                //idle state

                break;
            case (Battle_State.Charging):
                Update_ATB_Bars();
                break;
        }

       if (Hand_Indicator.activeSelf)
        {
            SetHand();
        }
	
	}



    public void Update_ATB_Bars()
    {
        Croix_ATB = Croix_ATB + Time.deltaTime * Croix_Speed;
        Croix_ATB_Bar.value = Croix_ATB;

        if (Croix_ATB >= Croix_Max_ATB)
        {
            current_state = Battle_State.Waiting;
            StartCoroutine(PlayerTurnStarts());
        }

        else
        {
            Enemy_ATB = Enemy_ATB + Time.deltaTime * Enemy_Speed;
            Enemy_ATB_Bar.value = Enemy_ATB;
            if (Enemy_ATB >= Enemy_Max_ATB)
            {
                current_state = Battle_State.Waiting;
                StartCoroutine(EnemyActs());
            }
        }
    }

    IEnumerator EnemyActs()
    {
        if (Current_Whirlwind_Slow > 0)
        {
           
          if (Current_Whirlwind_Slow == 1)
            {
                Current_Whirlwind_Slow = 0;
                Enemy_Speed = Enemy_Max_Speed;
            }
          else
            {
                Current_Whirlwind_Slow--;
            }
        }

        if (Current_GuardUp_Duration > 0)
        {
            Current_GuardUp_Duration--;
        }

        if (Current_Lightguard_Duration > 0)
        {
            Current_Lightguard_Duration--;
        }

        if (FireBall_Current_Cooldown > 0)
        {
            FireBall_Current_Cooldown--;
        }

        if (GuardUp_Current_Cooldown > 0)
        {
            GuardUp_Current_Cooldown--;
        }

        if (JusticeStrike_Current_Cooldown > 0)
        {
            JusticeStrike_Current_Cooldown--;
        }

        if (StarBlazing_Current_Cooldown > 0)
        {
            StarBlazing_Current_Cooldown--;
        }

        if (Determination_Current_Cooldown > 0)
        {
            Determination_Current_Cooldown--;
        }

        if (LightGuard_Current_Cooldown > 0)
        {
            LightGuard_Current_Cooldown--;
        }


        StartCoroutine(EnemyAttacks());
      
        yield break;
    }

    IEnumerator EnemyAttacks()
    {

        if (charging_justice_strike == true)
        {
            StartCoroutine(Enemy_Justice_Strike_2());
        }
        else
        {
            int Random_Number;

            if (Enemy_Phase == 1)
            {
                Random_Number = Random.Range(1, 4);

                switch (Random_Number)
                {
                    case 1:
                        StartCoroutine(Enemy_Slash());
                        break;

                    case 2:
                        if (FireBall_Current_Cooldown == 0)
                        {
                            StartCoroutine(Enemy_Fireball());
                        }
                        else
                        {
                            StartCoroutine(EnemyAttacks());
                        }
                        break;

                    case 3:
                        if (GuardUp_Current_Cooldown == 0 && Current_GuardUp_Duration == 0)
                        {
                            StartCoroutine(Enemy_Guard_Up());
                        }
                        else
                        {
                            StartCoroutine(EnemyAttacks());
                        }
                        break;
                }
            }

            else if (Enemy_Phase == 2)
            {
                Random_Number = Random.Range(1, 6);
                switch (Random_Number)
                {
                    case 1:
                        StartCoroutine(Enemy_Neo_Blades());
                        break;

                    case 2:
                        if (JusticeStrike_Current_Cooldown == 0)
                        {
                            StartCoroutine(Enemy_Justice_Strike());
                        }

                        else
                        {
                            StartCoroutine(EnemyAttacks());
                        }
                        break;

                    case 3:
                        if (LightGuard_Current_Cooldown == 0 && Current_Lightguard_Duration == 0)
                        {
                            StartCoroutine(Enemy_Light_Guard());
                        }

                        else
                        {
                            StartCoroutine(EnemyAttacks());
                        }
                        break;

                    case 4:
                        if (StarBlazing_Current_Cooldown == 0)
                        {
                            StartCoroutine(Enemy_Star_Blazing());
                        }
                        else
                        {
                            StartCoroutine(EnemyAttacks());
                        }
                        break;

                    case 5:
                        if (Enemy_Health < ((Enemy_Max_Health * 40) / 100) && Determination_Current_Cooldown == 0)
                        {
                            StartCoroutine(Enemy_Determination());
                        }

                        else
                        {
                            StartCoroutine(EnemyAttacks());
                        }
                        break;

                }

            }
        }

        yield break;
    }

    IEnumerator PlayerTurnStarts()
    {

        player_is_guarding = false;
        GameObject.Find("Menu_Ready").GetComponent<AudioSource>().Play();

        if (Current_Fireball_Duration > 0)
        {
            Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.right;
            Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
            Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
            Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
            Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Croix loses HP because of being burnt! (" + Current_Fireball_Duration.ToString() + " turns remaining";
            Vector3 auxiliar = Enemy.transform.localPosition;
            auxiliar.x -= Notification_text_distancetoaddx;
            auxiliar.y += Notification_text_distancetoaddy;
            GameObject temp = Instantiate(Notification_Text, auxiliar, Croix.transform.rotation) as GameObject;
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            temp.transform.localPosition = auxiliar;

            Croix_Health -= Mathf.FloorToInt((Croix_Max_Health * 8) / 100);
            DamageNumber.GetComponent<Text>().text = Mathf.FloorToInt((Croix_Max_Health * 8) / 100).ToString();
            temp = Instantiate(DamageNumber, Croix.transform.position, Croix.transform.rotation) as GameObject;
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            Destroy(temp, 2.0f);
            Croix_HP_Bar.value = Croix_Health;

            //StartCoroutine(DealDamage(Croix, Mathf.FloorToInt((Croix_Max_Health * 8) / 100)));
            Current_Fireball_Duration--;
            //yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);
        }

        if (Current_StarBlazing_Duration > 0)
        {
            Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.right;
            Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
            Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
            Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
            Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Croix loses HP because of being burnt! (" + Current_StarBlazing_Duration.ToString() + " turns remaining";
            Vector3 auxiliar = Enemy.transform.localPosition;
            auxiliar.x -= Notification_text_distancetoaddx;
            auxiliar.y += Notification_text_distancetoaddy;
            GameObject temp = Instantiate(Notification_Text, auxiliar, Croix.transform.rotation) as GameObject;
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            temp.transform.localPosition = auxiliar;

            Croix_Health -= Mathf.FloorToInt((Croix_Max_Health * 15) / 100);
            DamageNumber.GetComponent<Text>().text = Mathf.FloorToInt((Croix_Max_Health * 15) / 100).ToString();
            temp = Instantiate(DamageNumber, Croix.transform.position, Croix.transform.rotation) as GameObject;
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            Destroy(temp, 2.0f);
            Croix_HP_Bar.value = Croix_Health;
            Current_StarBlazing_Duration--;
            //yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);
        }

     
        if (Guard_Crush_Current_Cooldown > 0)
        {
            Guard_Crush_Current_Cooldown--;
        }

        if (Whirlwind_Current_Cooldown > 0)
        {
            Whirlwind_Current_Cooldown--;
        }

        if (Meteor_Current_Cooldown > 0)
        {
            Meteor_Current_Cooldown--;
        }

        if (Impact_Swing_Current_Cooldown > 0)
        {
            Impact_Swing_Current_Cooldown--;
        }

        if (Accelerate_Current_Cooldown > 0)
        {
            Accelerate_Current_Cooldown--;
        }

        if (Deccelerate_Current_Cooldown > 0)
        {
            Deccelerate_Current_Cooldown--;
        }

        if (Guard_Current_Cooldown > 0)
        {
            Guard_Current_Cooldown--;
        }

        Croix_Menu.SetActive(true);
        Croix_Menu.transform.Find("Attack").GetComponent<Button>().Select();
        Hand_Indicator.SetActive(true);
        Croix_Information_Panel.SetActive(true);
        yield break;
    }























    //Auxiliar methods for acting

    IEnumerator Enemy_Slash()
    {
        yield return StartCoroutine(DealDamage(Croix, 12));
        Start();
        Enemy_ATB = 0;
    }

    IEnumerator Enemy_Fireball()
    {
        StartCoroutine(DealDamage(Croix, 25));
        Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.left;
        Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
        Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
        Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
        Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Croix is set ablaze! Loses HP every turn.";
        Vector3 auxiliar = Enemy.transform.localPosition;
        auxiliar.x += Notification_text_distancetoaddx;
        auxiliar.y += Notification_text_distancetoaddy;

        GameObject temp = Instantiate(Notification_Text, auxiliar, Enemy.transform.rotation) as GameObject;
        temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
        temp.transform.localScale = new Vector3(1, 1, 1);
        temp.transform.localPosition = auxiliar;

        yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);
        Current_Fireball_Duration = Max_Fireball_Duration;

        FireBall_Current_Cooldown = FireBall_Max_Cooldown;
        Start();
        Enemy_ATB = 0;
    }

    IEnumerator Enemy_Guard_Up()
    {
        Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.left;
        Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
        Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
        Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
        Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Silas Guards! Reduces damage taken for 8 turns!";
        Vector3 auxiliar = Enemy.transform.localPosition;
        auxiliar.x += Notification_text_distancetoaddx;
        auxiliar.y += Notification_text_distancetoaddy;

        GameObject temp = Instantiate(Notification_Text, auxiliar, Enemy.transform.rotation) as GameObject;
        temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
        temp.transform.localScale = new Vector3(1, 1, 1);
        temp.transform.localPosition = auxiliar;

        yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);
        Current_GuardUp_Duration = Max_GuardUp_Duration;

        GuardUp_Current_Cooldown = GuardUp_Max_Cooldown;
        Start();
        Enemy_ATB = 0;
    }

    IEnumerator Enemy_Neo_Blades()
    {
        StartCoroutine(DealDamage(Croix, 25));
        Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.left;
        Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
        Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
        Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
        Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Silas uses Neo Blades!";
        Vector3 auxiliar = Enemy.transform.localPosition;
        auxiliar.x += Notification_text_distancetoaddx;
        auxiliar.y += Notification_text_distancetoaddy;

        GameObject temp = Instantiate(Notification_Text, auxiliar, Enemy.transform.rotation) as GameObject;
        temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
        temp.transform.localScale = new Vector3(1, 1, 1);
        temp.transform.localPosition = auxiliar;

        yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);


        Start();
        Enemy_ATB = 0;
        yield break;
    }

    IEnumerator Enemy_Justice_Strike()
    {
        Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.left;
        Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
        Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
        Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
        Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Silas is readying Justice Strike! It will strike next turn.";
        Vector3 auxiliar = Enemy.transform.localPosition;
        auxiliar.x += Notification_text_distancetoaddx;
        auxiliar.y += Notification_text_distancetoaddy;

        GameObject temp = Instantiate(Notification_Text, auxiliar, Enemy.transform.rotation) as GameObject;
        temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
        temp.transform.localScale = new Vector3(1, 1, 1);
        temp.transform.localPosition = auxiliar;

        charging_justice_strike = true;
        yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);

        Start();
        Enemy_ATB = 0;
        yield break;
    }

    IEnumerator Enemy_Justice_Strike_2()
    {
        StartCoroutine(DealDamage(Croix, 100));
        Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.left;
        Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
        Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
        Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
        Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Silas uses Justice Strike!";
        Vector3 auxiliar = Enemy.transform.localPosition;
        auxiliar.x += Notification_text_distancetoaddx;
        auxiliar.y += Notification_text_distancetoaddy;

        GameObject temp =  Instantiate(Notification_Text, auxiliar, Enemy.transform.rotation) as GameObject;
        temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
        temp.transform.localScale = new Vector3(1, 1, 1);
        temp.transform.localPosition = auxiliar;

        yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);

        charging_justice_strike = false;
        JusticeStrike_Current_Cooldown = JusticeStrike_Max_Cooldown;
        Start();
        Enemy_ATB = 0;
        yield break;
    }

    IEnumerator Enemy_Light_Guard()
    {
        Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.left;
        Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
        Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
        Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
        Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Silas uses Light Guard! Reduces damage taken for 8 turns!";
        Vector3 auxiliar = Enemy.transform.localPosition;
        auxiliar.x += Notification_text_distancetoaddx;
        auxiliar.y += Notification_text_distancetoaddy;

        GameObject temp = Instantiate(Notification_Text, auxiliar, Enemy.transform.rotation) as GameObject;
        temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
        temp.transform.localScale = new Vector3(1, 1, 1);
        temp.transform.localPosition = auxiliar;

        yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);

        Current_Lightguard_Duration = Max_Lightguard_Duration;

        LightGuard_Current_Cooldown = LightGuard_Max_Cooldown;
        Start();
        Enemy_ATB = 0;
    }

    IEnumerator Enemy_Star_Blazing()
    {
        StartCoroutine(DealDamage(Croix, 30));
        Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.left;
        Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
        Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
        Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
        Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Sias uses Star Blazing! Croix is set ablaze!";
        Vector3 auxiliar = Enemy.transform.localPosition;
        auxiliar.x += Notification_text_distancetoaddx;
        auxiliar.y += Notification_text_distancetoaddy;

        GameObject temp = Instantiate(Notification_Text, auxiliar, Enemy.transform.rotation) as GameObject;
        temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
        temp.transform.localScale = new Vector3(1, 1, 1);
        temp.transform.localPosition = auxiliar;

        yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);

        StarBlazing_Current_Cooldown = StarBlazing_Max_Cooldown;
        Current_StarBlazing_Duration = Max_StarBlazing_Duration;
        Enemy_ATB = 0;
        Start();
    }

    IEnumerator Enemy_Determination()
    {
        Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.left;
        Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
        Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
        Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
        Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Silas uses Determination to heal himself!";
        Vector3 auxiliar = Enemy.transform.localPosition;
        auxiliar.x += Notification_text_distancetoaddx;
        auxiliar.y += Notification_text_distancetoaddy;

        GameObject temp = Instantiate(Notification_Text, auxiliar, Enemy.transform.rotation) as GameObject;
        temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
        temp.transform.localScale = new Vector3(1, 1, 1);
        temp.transform.localPosition = auxiliar;

        if (Enemy_Health < (Enemy_Max_Health * 90)/100)
        {
            Enemy_Health += Mathf.Floor((Enemy_Max_Health * 10) / 100);
        }
        else
        {
            Enemy_Health = Enemy_Max_Health;
        }

        Enemy_HP_Bar.value = Enemy_Health;
        yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);

        Enemy_ATB = 0;
        Start();
        Determination_Current_Cooldown = Determination_Max_Cooldown;
    }


    IEnumerator ExecuteAttack()
    {

        yield return new WaitForSeconds(2.0f);
        yield return DealDamage(Enemy, 16);
        Enemy_ATB -= 15;
        if (Enemy_ATB < 0)
        {
            Enemy_ATB = 0;
        }

        Croix_ATB = 0;
        Start();

    }

    IEnumerator Croix_Guard_Crush()
    {
        if (Current_GuardUp_Duration > 0)
        {
            Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.right;
            Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
            Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
            Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
            Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Croix crushes Silas' guard! He no longer receives reduced damage!";
            Vector3 auxiliar = Enemy.transform.localPosition;
            auxiliar.x -= Notification_text_distancetoaddx;
            auxiliar.y += Notification_text_distancetoaddy;

            GameObject temp = Instantiate(Notification_Text, auxiliar, Croix.transform.rotation) as GameObject;
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            temp.transform.localPosition = auxiliar;

            Current_GuardUp_Duration = 0;
        }

        if (Current_Lightguard_Duration > 0)
        {
            Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.right;
            Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
            Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
            Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
            Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Croix crushes Silas' guard! He no longer receives reduced damage!";
            Vector3 auxiliar = Enemy.transform.localPosition;
            auxiliar.x -= Notification_text_distancetoaddx;
            auxiliar.y += Notification_text_distancetoaddy;

            GameObject temp = Instantiate(Notification_Text, auxiliar, Croix.transform.rotation) as GameObject;
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            temp.transform.localPosition = auxiliar;

            Current_Lightguard_Duration = 0;
        }
        yield return StartCoroutine(DealDamage(Enemy, 20));

        Guard_Crush_Current_Cooldown = Guard_Crush_Max_Cooldown;

        Croix_ATB = 0;
        Start();
    }

    IEnumerator Croix_Whirlwind()
    {
        Notification_Text.GetComponent<Notifications_Text_Script>().direction = Vector3.right;
        Notification_Text.GetComponent<Notifications_Text_Script>().speed = Notification_text_Speed;
        Notification_Text.GetComponent<Notifications_Text_Script>().lifetime = Notification_text_Lifetime;
        Notification_Text.GetComponent<Notifications_Text_Script>().initial_time = Notification_text_initial_time;
        Vector3 auxiliar = Enemy.transform.localPosition;
        auxiliar.x -= Notification_text_distancetoaddx;
        auxiliar.y += Notification_text_distancetoaddy;

        Enemy_ATB -= 60;
        if (Enemy_ATB < 0)
        {
            Enemy_ATB = 0;
        }

        yield return new WaitForSeconds(Notification_Text.GetComponent<Notifications_Text_Script>().lifetime);

        if (Current_Whirlwind_Slow > 0)
        {
            Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Croix uses Whirlwind! Silas' slow refreshes!";
            GameObject temp = Instantiate(Notification_Text, auxiliar, Croix.transform.rotation) as GameObject;
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            temp.transform.localPosition = auxiliar;
        }

        else
        {
            Notification_Text.transform.Find("Text").GetComponent<Text>().text = "Croix uses Whirlwind! Silas is slowed!";
            GameObject temp = Instantiate(Notification_Text, auxiliar, Croix.transform.rotation) as GameObject;
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            temp.transform.localPosition = auxiliar;
            Enemy_Speed = (Enemy_Max_Speed * 70) / 100;
        }
        yield return StartCoroutine(DealDamage(Enemy, 10));

        Current_Whirlwind_Slow = Max_Whirlwind_Slow;
        Debug.Log(Current_Whirlwind_Slow);
        Whirlwind_Current_Cooldown = Whirlwind_Max_Cooldown;

        Croix_ATB = 0;
        Start();
    }

    IEnumerator Croix_Meteor()
    {

        Enemy_ATB -= 40;
        if (Enemy_ATB < 0)
        {
            Enemy_ATB = 0;
        }

        yield return StartCoroutine(DealDamage(Enemy, 60));

        Meteor_Current_Cooldown = Meteor_Max_Cooldown;

        Croix_ATB = 0;
        Start();
    }

    IEnumerator Croix_Impact_Swing()
    {
        Enemy_ATB -= 100;
        if (Enemy_ATB < 0)
        {
            Enemy_ATB = 0;
        }

        yield return StartCoroutine(DealDamage(Enemy, 30));

        Impact_Swing_Current_Cooldown = Impact_Swing_Max_Cooldown;

        Croix_ATB = 0;
        Start();
    }

    IEnumerator Croix_Accelerate()
    {

        Croix_Deffense -= Croix_Base_Deffense;
        Croix_Speed += Croix_Base_Speed * 25 / 100;
        Croix_Acceleration++;

        Croix_ATB = 0;
        Start();

        yield break;
    }

    IEnumerator Croix_Deccelerate()
    {
        Croix_Deffense += Croix_Base_Deffense;
        Croix_Speed -= Croix_Base_Speed * 25 / 100;
        Croix_Acceleration--;

        Croix_ATB = 0;
        Start();

        yield break;
    }


    public void SetHand()
    {
        GameObject myEventSystem = GameObject.Find("EventSystem");
        GameObject currentbutton = myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().currentSelectedGameObject;
        Hand_Indicator.transform.position = new Vector3(currentbutton.transform.position.x + 1.5f, currentbutton.transform.position.y, Hand_Indicator.transform.position.z);
    }

    public void SubmitAttack()
    {
        Croix_Menu.SetActive(false);
        Croix_Skills.SetActive(false);
        Croix_Items.SetActive(false);
        Croix_Information_Panel.SetActive(false);
        Hand_Indicator.SetActive(false);
        StartCoroutine(ExecuteAttack());
    }


    public void SubmitSpecial()
    {
        Croix_Menu.SetActive(false);
        Croix_Skills.SetActive(true);
        Croix_Skills.transform.Find("Skills_Spacer").transform.Find("Guard Crush").GetComponent<Button>().Select();
    }

    public void SubmitItem()
    {
        Croix_Menu.SetActive(false);
        Croix_Items.SetActive(true);
        Croix_Items.transform.Find("Items_Spacer").transform.Find("Potions").GetComponent<Button>().Select();
        Croix_Items.transform.Find("Items_Spacer").transform.Find("Potions").transform.Find("Text").GetComponent<Text>().text = "Potions x "+potions.ToString();
        Croix_Items.transform.Find("Items_Spacer").transform.Find("Herbs").transform.Find("Text").GetComponent<Text>().text = "Herbs x " + herbs.ToString();
    }

    public void SubmitGuard()
    {
        if (Guard_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "This skill is on cooldown! " + Guard_Current_Cooldown.ToString() + " turns remaining.";
        }
        else
        {
            player_is_guarding = true;
            Croix_ATB = 0;
            Start();
        }
    }

    public void CancelAttack()
    {
        Croix_Menu.transform.Find("Attack").GetComponent<Button>().Select();
    }

    public void CancelSpecial()
    {

        Croix_Menu.transform.Find("Special").GetComponent<Button>().Select();
    }

    public void CancelItem()
    {
        Croix_Menu.transform.Find("Item").GetComponent<Button>().Select();
    }

    public void CancelGuard()
    {
        Croix_Menu.transform.Find("Guard").GetComponent<Button>().Select();
    }

    public void SelectAttack()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Basic attack. Deals 16 damage, reduces the enemy ATB by 10 and has no cooldown.";
    }

    public void SelectSpecial()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Use one of Croix's special skills.";
    }

    public void SelectItem()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Use one of Croix's items.";
    }

    public void SelectGuard()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Increase your defense until next turn. " + Guard_Max_Cooldown.ToString()+ " turns cooldown.";
        if (Guard_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text += "\n Currently on cooldown. " + Guard_Current_Cooldown.ToString() + " turns remaining";
        }
    }

    public void SubmitGuardCrush()
    {
        if (Guard_Crush_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "This skill is on cooldown! " + Guard_Crush_Current_Cooldown.ToString() + " turns remaining.";
        }
        else
        {
            Croix_Menu.SetActive(false);
            Croix_Skills.SetActive(false);
            Croix_Items.SetActive(false);
            Croix_Information_Panel.SetActive(false);
            Hand_Indicator.SetActive(false);
            StartCoroutine(Croix_Guard_Crush());
        }
    }

    public void SubmitWhirlwind()
    {
        if (Whirlwind_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "This skill is on cooldown! " + Whirlwind_Current_Cooldown.ToString() + " turns remaining.";
        }
        else
        {
            Croix_Menu.SetActive(false);
            Croix_Skills.SetActive(false);
            Croix_Items.SetActive(false);
            Croix_Information_Panel.SetActive(false);
            Hand_Indicator.SetActive(false);
            StartCoroutine(Croix_Whirlwind());
        }

    }

    public void SubmitMeteor()
    {
        if (Meteor_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "This skill is on cooldown! " + Meteor_Current_Cooldown.ToString() + " turns remaining.";
        }
        else if (Croix_Acceleration != 2)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Must be at maximum Acceleration (2) to use! Current acceleration: " + Croix_Acceleration.ToString();
        }
        else
        {
            Croix_Menu.SetActive(false);
            Croix_Skills.SetActive(false);
            Croix_Items.SetActive(false);
            Croix_Information_Panel.SetActive(false);
            Hand_Indicator.SetActive(false);
            StartCoroutine(Croix_Meteor());
        }
    }

    public void SubmitImpact_Swing()
    {
        if (Impact_Swing_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "This skill is on cooldown! " + Impact_Swing_Current_Cooldown.ToString() + " turns remaining.";
        }
        else if (Croix_Acceleration != 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Must be at minimum Acceleration (0) to use! Current acceleration: " + Croix_Acceleration.ToString();
        }
        else
        {
            Croix_Menu.SetActive(false);
            Croix_Skills.SetActive(false);
            Croix_Items.SetActive(false);
            Croix_Information_Panel.SetActive(false);
            Hand_Indicator.SetActive(false);
            StartCoroutine(Croix_Impact_Swing());
        }

    }

    public void SubmitAccelerate()
    {
        if (Accelerate_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "This skill is on cooldown! " + Accelerate_Current_Cooldown.ToString() + " turns remaining.";
        }
        else if (Croix_Acceleration < 2)
        {
            Croix_Menu.SetActive(false);
            Croix_Skills.SetActive(false);
            Croix_Items.SetActive(false);
            Croix_Information_Panel.SetActive(false);
            Hand_Indicator.SetActive(false);
            StartCoroutine(Croix_Accelerate());
        }
        else
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Already at maximum acceleration!";
        }
    }

    public void SubmitDeccelerate()
    {
        if (Deccelerate_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "This skill is on cooldown! " + Deccelerate_Current_Cooldown.ToString() + " turns remaining.";
        }
        else if (Croix_Acceleration > 0)
        {
            Croix_Menu.SetActive(false);
            Croix_Skills.SetActive(false);
            Croix_Items.SetActive(false);
            Croix_Information_Panel.SetActive(false);
            Hand_Indicator.SetActive(false);
            StartCoroutine(Croix_Deccelerate());
        }
        else
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Already at minimum acceleration!";
        }
    }

    public void CancelInsideSpecial()
    {
        Croix_Menu.SetActive(true);
        Croix_Skills.SetActive(false);
        Croix_Menu.transform.Find("Special").GetComponent<Button>().Select();
    }

    public void SelectGuardCrush()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Breaks enemy guards. Deals 20 damage,  " + Guard_Crush_Max_Cooldown.ToString() + " turns cooldown.";
        if (Guard_Crush_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text += "\n Currently on cooldown. " + Guard_Crush_Current_Cooldown.ToString() + " turns remaining";
        }
    }

    public void SelectWhirlwind()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Reduces enemy ATB by 50 and slows them for 5 turns. " + Whirlwind_Max_Cooldown.ToString() + " turns cooldown.";
        if (Whirlwind_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text += "\n Currently on cooldown. " + Whirlwind_Current_Cooldown.ToString() + " turns remaining";
        }
    }

    public void SelectMeteor()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Must be at full speed to use. 50 damage, reduces enemy ATB by 100.  " + Meteor_Max_Cooldown.ToString() + " turns cooldown.";
        if (Meteor_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text += "\n Currently on cooldown. " + Meteor_Current_Cooldown.ToString() + " turns remaining";
        }
    }

    public void SelectImpactSwing()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Must be at lowest speed to use. 30 damage, reduces enemy ATB by 20.  " + Impact_Swing_Max_Cooldown.ToString() + " turns cooldown.";
        if (Impact_Swing_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text += "\n Currently on cooldown. " + Impact_Swing_Current_Cooldown.ToString() + " turns remaining";
        }
    }

    public void SelectAccelerate()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Increases your speed at the cost of defense.  " + Accelerate_Max_Cooldown.ToString() + " turns cooldown.";
        if (Accelerate_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text += "\n Currently on cooldown. " + Accelerate_Current_Cooldown.ToString() + " turns remaining";
        }
    }

    public void SelectDeccelerate()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Increases your speed at the cost of defense.  " + Deccelerate_Max_Cooldown.ToString() + " turns cooldown.";
        if (Deccelerate_Current_Cooldown > 0)
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text += "\n Currently on cooldown. " + Deccelerate_Current_Cooldown.ToString() + " turns remaining";
        }
    }

    public void SubmitPotion()
    {
        Croix_Menu.SetActive(false);
        Croix_Skills.SetActive(false);
        Croix_Items.SetActive(false);
        Croix_Information_Panel.SetActive(false);
        Hand_Indicator.SetActive(false);
        if (potions > 0)
        {
            Croix_Health += Mathf.Floor(Croix_Max_Health / 2);

            if (Croix_Health > Croix_Max_Health)
            {
                Croix_Health = Croix_Max_Health;
            }

            Croix_HP_Bar.value = Croix_Health;
            potions--;
        }

        else
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "You have no remaining potions!";
        }

        Croix_ATB = 0;
        Start();
    }

    public void SubmitHerb()
    {
        if (herbs > 0)
        {
            if (Current_StarBlazing_Duration > 0)
            {
                Current_StarBlazing_Duration = 0;
            }

            if (Current_Fireball_Duration > 0)
            {
                Current_Fireball_Duration = 0;
            }

            herbs--;
        }
       
        else
        {
            Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "You have no remaining herbs!";

        }

        Croix_Menu.SetActive(false);
        Croix_Skills.SetActive(false);
        Croix_Items.SetActive(false);
        Croix_Information_Panel.SetActive(false);
        Hand_Indicator.SetActive(false);

        Croix_ATB = 0;
        Start();
    }

    public void CancelInsideItem()
    {
        Croix_Menu.SetActive(true);
        Croix_Items.SetActive(false);
        Croix_Menu.transform.Find("Item").GetComponent<Button>().Select();
    }

    public void SelectPotion()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Using a potion recovers half of your health.";
    }

    public void SelectHerb()
    {
        Croix_Information_Panel.transform.Find("Text").GetComponent<Text>().text = "Using a herb cures status ailments.";
    }   

    public void DeselectGlobal()
    {

    }


    IEnumerator DealDamage(GameObject target, int damage_amount)
    {
        if (target == Croix)
        {
            if (player_is_guarding == true)
            {
                float auxiliary_damage = ((float)damage_amount) / 3;
                damage_amount = Mathf.FloorToInt(auxiliary_damage);
            }

            damage_amount = Mathf.FloorToInt((float)damage_amount * (100 - Croix_Deffense) / 100);

            if (damage_amount < 0)
            {
                damage_amount = 0;
            }
            DamageNumber.GetComponent<Text>().text = damage_amount.ToString();
            GameObject temp = Instantiate(DamageNumber, Croix.transform.position, Croix.transform.rotation) as GameObject;
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            Destroy(temp, 2.0f);
            
            Croix_Health -= damage_amount;
            Croix_HP_Bar.value = Croix_Health;

            if (Croix_Health < 0)
            {
                Croix_Health = 0;
                Croix_HP_Bar.value = Croix_Health;
                StartCoroutine(Game_Over());
            }

        }

        else if (target == Enemy)
        {
            if (Current_GuardUp_Duration > 0)
            {
                float auxiliary_damage = ((float)damage_amount) / 2;
                damage_amount = Mathf.FloorToInt(auxiliary_damage);
            }

            if (Current_Lightguard_Duration > 0)
            {
                float auxiliary_damage = ((float)damage_amount * 20 / 100);
                damage_amount = Mathf.FloorToInt(auxiliary_damage);
            }

            DamageNumber.GetComponent<Text>().text = damage_amount.ToString();
            GameObject temp = Instantiate(DamageNumber, Enemy.transform.position, Enemy.transform.rotation) as GameObject;   
            temp.transform.SetParent(GameObject.Find("BattleCanvas").transform);
            temp.transform.localScale = new Vector3(1, 1, 1);
            Destroy(temp, 2.0f);
            Enemy_Health -= damage_amount;
            Enemy_HP_Bar.value = Enemy_Health;

            if (Enemy_Health < 0)
            {
                Enemy_Health = 0;
                Enemy_HP_Bar.value = Enemy_Health;
                
                if (Enemy_Phase == 2)
                {
                    StartCoroutine(Battle_Won());
                }

                else if (Enemy_Phase == 1)
                {
                    StartCoroutine(Start_Phase_2());
                }

            }
        }
        yield return new WaitForSeconds(2.0f);
        yield break;
    }

    IEnumerator Game_Over()
    {
        yield break;
    }

    IEnumerator Start_Phase_2()
    {
        Current_GuardUp_Duration = 0;
        //Current_Fireball_Duration = 0;
        Current_Whirlwind_Slow = 0;

        Enemy_Max_Health = 250;
        Enemy_Health = 250;
        Enemy_HP_Bar.maxValue = 250;
        Enemy_HP_Bar.value = 250;

        Enemy_Phase = 2;

        yield break;
    }

    IEnumerator Battle_Won()
    {
        yield break;
    }
}
