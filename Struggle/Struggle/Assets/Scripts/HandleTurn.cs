﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class HandleTurn {

    public string Attacker; //name of attacker
    public string Type;
    public GameObject AttackersGameObject; //who attacks
    public GameObject AttackersTarget; //who's going to be attacked

    //Which attack is performed
    public BaseAttack chosenAttack;


}
