﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BaseAttack:MonoBehaviour
{
    public string attackName;
    public string attackDescription;
    public float attackDamage; //Base Damage 15, melee lvl 10 stamina 15 = basedmg + stamina + lvl;
    public float attackCost; //ManaCost
}
