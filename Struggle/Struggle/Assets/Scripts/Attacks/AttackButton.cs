﻿using UnityEngine;
using System.Collections;

public class AttackButton : MonoBehaviour {

    public BaseAttack magicAttackToPerform;

    public void CastMagicAttack()
    {
        GameObject.Find("BattleManager").GetComponent < BattleStateMachine > ().Input4(magicAttackToPerform);
    }

}
