﻿using UnityEngine;
using System.Collections;

public class Poison1Spell : BaseAttack {

	public Poison1Spell()
    {
        attackName = "Poison 1";
        attackDescription = "Basic Poison Spell";
        attackDamage = 5f;
        attackCost = 5f;
    }
}
