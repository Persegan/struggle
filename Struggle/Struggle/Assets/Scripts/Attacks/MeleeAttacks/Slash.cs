﻿using UnityEngine;
using System.Collections;

public class Slash : BaseAttack
{
    public Slash()
    {
        attackName = "Slash";
        attackDescription = "Normal slashing attack";
        attackDamage = 10f;
        attackCost = 0f;
    }

}
