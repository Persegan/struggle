﻿using UnityEngine;
using System.Collections;

public class HammerSwing : BaseAttack
{

    public HammerSwing()
    {
        attackName = "HammerSwing";
        attackDescription = "Swing your hammer";
        attackDamage = 15f;
        attackCost = 0;
    }

}
