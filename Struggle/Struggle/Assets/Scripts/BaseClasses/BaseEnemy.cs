﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class BaseEnemy:BaseClass {



    public enum Type
    {
        GRASS,
        FIRE,
        WATER,
        ELECTRIC
    }

    public enum Rarity
    {
        COMMON,
        UNCOMMON,
        RARE,
        SUPERRARE

    }

    public Type EnemyType;
    public Rarity rarity;

}
