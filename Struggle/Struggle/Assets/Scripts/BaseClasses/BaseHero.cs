﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BaseHero:BaseClass {

    public int stamina;
    public int intellect;
    public int dexterity;
    public int agility;


    public List<BaseAttack> MagicAttacks = new List<BaseAttack>();
    	
}
