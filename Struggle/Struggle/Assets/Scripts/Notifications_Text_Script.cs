﻿using UnityEngine;
using System.Collections;

public class Notifications_Text_Script : MonoBehaviour {

    public float lifetime;
    public float initial_time;
    public float speed;
    public Vector3 direction;


    public enum State
    {
        Phase1,
        Phase2
    }

    public State current_state;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, lifetime);
        current_state = State.Phase1;
        StartCoroutine(phase1());
	
	}
	
	// Update is called once per frame
	void Update () {

        switch (current_state)
        {
            case (State.Phase1):
                transform.position += direction * Time.deltaTime * speed;

                break;

            case (State.Phase2):

                break;
        }
	
	}

    IEnumerator phase1()
    {
        yield return new WaitForSeconds(initial_time);
        current_state = State.Phase2;
    }
}
